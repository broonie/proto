init:
	
test: init
	python -m unittest

test-arlo: init
	python -m unittest -v test.test_arlo.TestArlo.integration_test_arlo

test-ch: init
	find test/ch ! -type d -delete 
	python -m unittest -v test.test_channel.TestChannel.integration_test_channel

test-audio: init
	python -m unittest -v test.test_audio.TestAudio.integration_test_audio

test-mark:
	python -m unittest -v test.test_mark.TestMark.mark_background

test-e: 
	python -m unittest -v test.test_events.TestEvents.integration_test_events


clean:
	rm -f src/__pycache__/*
	rm -f tests/__pycache__/*
	rm -rf lib/build
	
.PHONY: init test test-arlo test-bg clean

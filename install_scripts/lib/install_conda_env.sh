python_version=$(cat install_scripts/python_version)
python_major=$(echo $python_version | cut -d. -f1)
python_minor=$(echo $python_version | cut -d. -f2)
conda_env_name=proto-py$python_major$python_minor

echo "Installing conda environment..."

for i in {1..$(conda env list | wc -l)}; do conda deactivate; done
conda env remove -y --name $conda_env_name  > /dev/null 2>&1 || true
conda create -y --name $conda_env_name python=$python_version tensorflow-gpu keras

sed -i '\%conda activate%d' ~/.$(basename $SHELL)rc
echo "conda activate $conda_env_name" >> ~/.$(basename $SHELL)rc
source ~/.$(basename $SHELL)rc

python -m pip install --upgrade pip
python -m pip install numpy rx Arlo

unset python_version
unset python_major
unset python_minor
unset conda_env_name

root_dir=$(pwd)
build_dir=$root_dir/lib/build/opencv
python_env_path=$(dirname $(dirname $(which python3)))
python_env_lib_path=$python_env_path/lib/python$(cat install_scripts/python_version)

git submodule update --init --recursive
rm $build_dir -rf > /dev/null 2>&1 || true
mkdir -p $build_dir
cd $build_dir
cmake -DCMAKE_C_COMPILER=/usr/bin/gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/g++-6 -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF -DENABLE_PRECOMPILED_HEADERS=OFF -DOPENCV_ENABLE_NONFREE=ON -DWITH_CUDA=ON -DCUDA_HOST_COMPILER=/usr/bin/gcc-6 -DCUDA_NVCC_FLAGS=--expt-relaxed-constexpr -DWITH_TBB=ON -DWITH_OPENGL=ON -DWITH_OPENCL=OFF -DBUILD_JAVA=OFF -DWITH_VTK=OFF -DPYTHON3_EXECUTABLE=$python_env_path/bin/python -DOPENCV_EXTRA_MODULES_PATH=$root_dir/lib/opencv_contrib/modules $root_dir/lib/opencv $root_dir/lib/opencv
make -j $(cat /proc/cpuinfo | grep processor | wc -l)
cd $root_dir
rm $python_env_lib_path/cv2.so > /dev/null 2>&1 || true
ln -s $(find $build_dir/lib/python3 | tail -1) $python_env_lib_path/cv2.so

unset root_dir
unset build_dir
unset python_env_path
unset python_env_lib_path

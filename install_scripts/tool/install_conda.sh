latest_conda="Miniconda3-latest-Linux-x86_64.sh"

echo "Installing conda..."

rm -rf ~/miniconda3 > /dev/null 2>&1 || true
rm -rf ~/.conda > /dev/null 2>&1 || true
sed -i '\%conda3/etc/profile.d/conda.sh%d' ~/.$(basename $SHELL)rc

curl -O https://repo.continuum.io/miniconda/$latest_conda
$SHELL $latest_conda -b
rm $latest_conda
echo ". $(echo $HOME)/miniconda3/etc/profile.d/conda.sh" >> ~/.$(basename $SHELL)rc
source ~/.$(basename $SHELL)rc

unset latest_conda

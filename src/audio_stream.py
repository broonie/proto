import rx
from rx.subjects import Subject
from rx.concurrency import EventLoopScheduler

from threading import Thread
import asyncio
import websockets
from websockets.exceptions import ConnectionClosed

class AudioStream(Subject):
    def socket(port):
        return SyncStream(SocketStream(port))

    def __init__(self):
        super().__init__()

    def read(self) -> str:
        return list(self.first().to_blocking())[0]

    def write(self, text: str):
        pass

class SyncStream(AudioStream):
    def __init__(self, stream: AudioStream):
        super().__init__()
        self.stream = stream
        stream.observe_on(EventLoopScheduler()) \
            .subscribe(self)

    def write(self, text: str):
        self.stream.write(text)

class SocketStream(AudioStream):
    def __init__(self, port):
        super().__init__()
        self._port = port
        self._clients = set()
        self._thread = Thread(target=self._thread, daemon=True)
        self._thread.start()

    def _thread(self):
        async def handler(websocket, path):
            self._clients.add(websocket)
            async for text in websocket:
                self.on_next(text)

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(websockets.serve(handler, 'localhost', self._port))
        loop.run_forever()
        loop.close()

    def write(self, text: str):  
        async def safe_send(ws):
            try:
                await ws.send(text)
            except (ConnectionClosed):
                self._clients.remove(ws)

        loop = asyncio.new_event_loop()
        loop.run_until_complete(asyncio.wait([safe_send(ws) for ws in self._clients]))
        loop.close()

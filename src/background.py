import numpy as np
from .image import Image
from .mark import Mark
from functools import reduce
import cv2


#Determine largest contour in the image
def maxContour(contours):
    cnt_list = np.zeros(len(contours))
    for i in range(0,len(contours)):
        #contours[i] = cv2.approxPolyDP(contours[i], 0.001 * peri, False)
        cnt_list[i] = cv2.contourArea(contours[i], False)
        #cnt_list[i] = cv2.arcLength(contours[i], False)

    max_value = np.amax(cnt_list)
    max_index = np.argmax(cnt_list)

    cnt = contours[max_index]   

    return cnt, max_value


def centerOfMass(contour):
    nPoints = len(contour)

    x = 0
    y = 0

    for kp in contour:
        x = x + kp[0][0]
        y = y + kp[0][1]

    return x // nPoints, y // nPoints


def findMask(background, frame):
    scale = 3
    kernel = np.ones((3, 3), np.uint8)
    kernel7 = np.ones((9, 9), np.uint8)    

    height, width, channels = background.shape
    background = cv2.resize(background, (width//scale, height//scale), interpolation = cv2.INTER_LINEAR)
    frame = cv2.resize(frame, (width//scale, height//scale), interpolation = cv2.INTER_LINEAR)

    frame0 = frame.copy()
    #cv2.imshow('background', background)
    #cv2.imshow('frame', frame0)    

    #background = cv2.medianBlur(background, 3)
    #frame = cv2.medianBlur(frame0, 3)

    background = cv2.GaussianBlur(background, (15, 15), 3)
    frame = cv2.GaussianBlur(frame, (15, 15), 3)

    background = cv2.cvtColor(background, cv2.COLOR_BGR2HSV_FULL)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV_FULL)

    # fill shelves in black
    shelve_rect = np.array([[0, height//scale], [0, height//scale//1.9], [width, height//scale//1.9], [width, height//scale]], np.int32)
    cv2.fillConvexPoly(background, shelve_rect, (0, 0, 0))
    cv2.fillConvexPoly(frame, shelve_rect, (0, 0, 0))

    b, g, r = cv2.split(background)
    bf, gf, rf = cv2.split(frame)

    background = g
    frame = gf


    #cv2.imshow('background', background)
    #cv2.imshow('frame', frame)  

    diff =  cv2.absdiff(frame, background)
    #diff = cv2.convertScaleAbs(diff)

    #cv2.imshow('diff', diff)
    
    #diff = cv2.morphologyEx(diff, cv2.MORPH_CLOSE, kernel)
    #diff = cv2.morphologyEx(diff, cv2.MORPH_OPEN, kernel)
    #diff = cv2.GaussianBlur(diff, (15, 15), 5)
    #diff = cv2.GaussianBlur(diff, (3, 3), 1)
    #diff = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
    
    #cv2.imshow('diff', diff)

    _, mask = cv2.threshold(diff, 35, 255, cv2.THRESH_BINARY)

    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

    #cv2.imshow('mask', mask)

    mask_result = np.zeros((height//scale, width//scale, 1), np.uint8)
    

    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # only for test
    mask3 = np.zeros((height//scale, width//scale, 1), np.uint8)
    cv2.drawContours(mask3, contours, -1, 255, 1)
    #cv2.imshow('mask3', mask3)
    
    #_, contours, hierarchy = cv2.findContours(canny, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)  
    if len(contours) > 0:
        contour, length = maxContour(contours)
        if length > 300:
            cv2.drawContours(mask_result, [contour], 0, 255, 1)
            #cv2.drawContours(mask3, [contour], 0, 255, 2, cv2.LINE_AA)
            cv2.floodFill(mask_result, None, centerOfMass(contour), 255)
            mask_result = cv2.morphologyEx(mask_result, cv2.MORPH_CLOSE, kernel7)    

    # invert mask
    if (np.sum(mask_result == 255) > height//scale*width//scale//2):
        mask_result = 255 - mask_result

    # test    
    result = cv2.bitwise_and(frame0, frame0, mask = mask_result)
    #cv2.imshow('mask_result', mask_result)
    #cv2.imshow('result', result)

    mask_result = cv2.resize(mask_result, (width, height), interpolation = cv2.INTER_LINEAR)
    return mask_result


class Background:
    def __init__(self, image: Image):
        self.background = image.matrix
        self.mark = Mark.empty()

    def extract(self, image: Image) -> Image:        
        result = cv2.bitwise_and(image.matrix, image.matrix, mask = findMask(self.background, image.matrix))
        
        i = Image(result)
        i.mark = self.fillMark(result, image)
        return i 

    def fillMark(self, extracted_bg, image):
        imgray = cv2.cvtColor(extracted_bg, cv2.COLOR_BGR2GRAY)

        _, thresh = cv2.threshold(imgray, 1, 255, cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        if(len(contours) > 0):
            poly_contour = contours[0]
            return reduce(lambda mark, shelf: mark.polygon((0, 255, 0), image, shelf), poly_contour, Mark.empty())
        
        return Mark.empty()

from .product import Product

class Catalog:
    def __init__(self):
       self._products = []

    def match(self, product: Product) -> (Product, float):
        return max( \
            ((p, p.match(product)) for p in self._products), \
            key=lambda x: x[1], default=(Product.none(), -1))

    def add(self, new: Product) -> Product:
        self._products += [new]
        return new

    def replace(self, old: Product, new: Product) -> Product:
        self._products = [p if p != old else new for p in self._products]
        return new

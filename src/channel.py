from .location import Location
from .position import Position
from .product import Product
from .image import Image
from .console import Console
from .catalog import Catalog
from .tracker import Tracker, TrackerEvent, EnterEvent, ExitEvent, TouchEvent

import rx
from rx.subjects import Subject

Thresh = 0.55

class Channel(Subject):
    def __init__(self, tracker: Tracker, catalog: Catalog, console: Console):
        super().__init__()
        self._catalog = catalog
        self._console = console
        self._location = Location.outside()
        self._product = Product.none()
        self._name = None
        console.subscribe(self._on_name)
        tracker.subscribe(self._on_e)

    def _on_name(self, name: str):
        self._name = name

    def _on_e(self, e: TrackerEvent):
        if isinstance(e, EnterEvent) and e.photo is not None:
            self._product = self._sync(e.photo)

        if isinstance(e, TouchEvent):
            self._location = e.location            

        if isinstance(e, ExitEvent):
            if self._location != Location.outside():
                if e.photo is None: 
                    if self._product != Product.none():
                        self.on_next(PutEvent(self._product.name, self._location, e))
                else:
                    self._product = self._sync(e.photo)
                    self.on_next((TakeEvent(self._product.name, self._location, e)))

            self._name = None
            self._location = Location.outside()
            self._product = Product.none()
            
    def _sync(self, photo: Image) -> Product:
        product = Product.unknown().capture(photo)
        p, m = self._catalog.match(product)
        if m < Thresh:
            return self._catalog.add(product.define(self._name or self._console.prompt('Eh?')))                
        else:
            return self._catalog.replace(p, product.define(self._name or p.name).merge(p))


class ChannelEvent:
    def __init__(self, product: str, location: Location, src: TrackerEvent):
        super().__init__()
        self.product = product
        self.location = location
        self.src = src
        self.mark = src.mark

    def __str__(self):
        return '%s %s at %s' % (type(self).__name__, self.product, self.location)

class PutEvent(ChannelEvent):
    def __init__(self, product: str, location: Location, src: TrackerEvent):
        super().__init__(product, location, src)

class TakeEvent(ChannelEvent):
    def __init__(self, product: str, location: Location, src: TrackerEvent):
        super().__init__(product, location, src)


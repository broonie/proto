from threading import Thread

import rx
from rx.subjects import Subject
from rx.concurrency import EventLoopScheduler

from .audio_stream import AudioStream

class Console(Subject):
    def test(names = ['Pasta']):
        return TestConsole(names)

    def text():
        return SyncConsole(TextConsole())

    def audio(port):
        return SyncConsole(AudioConsole(port))
 
    def write(self, text: str) -> None:
        raise Exception('Not implemented.')

    def read(self):
        return list(self.first().to_blocking())[0]

    def prompt(self, text: str) -> str:
        self.write(text)
        return self.read()

class SyncConsole(Console):
    def __init__(self, console: Console):
        super().__init__()
        self._console = console
        console.observe_on(EventLoopScheduler()) \
            .subscribe(self)

    def write(self, text: str) -> None:
        self._console.write(text)

class ActiveConsole(Console):
    def __init__(self):
        super().__init__()
        self._thread = Thread(target=self._thread, daemon=True)
        self._thread.start()

    def _thread(self):
        while True:
            self.on_next(self._read())

    def _read(self) -> str:
        raise Exception('Not implemented.')

class TextConsole(ActiveConsole): 
    def __init__(self):
        super().__init__()

    def write(self, text: str) -> None:        
        print(text)

    def _read(self) -> str:
        while True:
            name = input('\nBROONIE> ')
            if name:
                return name

class AudioConsole(ActiveConsole):
    def __init__(self, port: int):
        self._stream = AudioStream.socket(port)
        super().__init__()
        
    def write(self, text: str) -> None:
        self._stream.write(text)

    def _read(self) -> str:
        return self._stream.read()
  
class TestConsole(Console):
    def __init__(self, names):
        super().__init__()
        self.names = names

    def write(self, text: str) -> None:
        print(text)

    def read(self):
        name = self.names.pop(0) if len(self.names) > 1 else self.names[0]
        self.on_next(name)
        print(name + '\n')
        return name

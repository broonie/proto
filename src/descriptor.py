from .image import Image
from .stopwatch import Stopwatch
from typing import Iterable
from typing import List
import cv2 as cv

DESCRIPTOR_SIZE = 256

# FLANN parameters
FLANN_INDEX_KDTREE = 1
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=50)   # or pass empty dictionary
flann = cv.FlannBasedMatcher(index_params,search_params)

class Descriptor:
    def none():
        return NullDescriptor()

    def sift(image: Image):
        return SiftDescriptor(image)

    def match(self, other) -> float:
        pass

    def __add__(self, other):
        return CompositeDescriptor([self, other])
        
class NullDescriptor(Descriptor):
    def match(self, other) -> float:
        return -1

class CompositeDescriptor(Descriptor):
    def __init__(self, descriptors):
        self.descriptors = descriptors

    def match(self, other) -> float:
        return max((d.match(other) for d in self.descriptors))

class SiftDescriptor(Descriptor):
    def __init__(self, image: Image):
        with Stopwatch('SIFT computing') as sw:
            sift = cv.xfeatures2d.SIFT_create(DESCRIPTOR_SIZE)    
            _, self._data = sift.detectAndCompute(image.matrix, None)

    def match(self, other) -> float:
        if not isinstance(other, SiftDescriptor):
            return other.match(self)

        with Stopwatch('FLANN match') as sw:
            matches = flann.knnMatch(self._data, other._data, k=2)

        good = 0
        for m,n in matches:
            if m.distance < 0.75*n.distance:
                good += 1
        
        return good / DESCRIPTOR_SIZE
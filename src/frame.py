from .image import Image
from .background import Background
from .layout import Layout
from .hand import Hand
from .location import Location
from .lazyprop import lazyprop

class Frame:
    def __init__(self, image: Image, bg: Background, layout: Layout, history: []):
        super().__init__()
        self.image = image
        self._bg = bg
        self._layout = layout
        self._history = history

    def __getitem__(self, i):
        return self._history[0 - i]

    @lazyprop
    def fg(self):
        return self._bg.extract(self.image)

    @lazyprop
    def hand(self):
        return Hand(self.image)

    @lazyprop
    def location(self) -> Location:
        return self._layout[self.hand.tip] if self.hand_found else Location.outside()

    @lazyprop
    def product(self):        
        return self.hand.clip(self.fg).cropped if self.hand_found and (self.fg.area / self.hand.area > 1.2) else None
 
    @lazyprop
    def mark(self) -> Image:
        mark = self.fg.mark + self.hand.mark + self._layout.mark
        return mark \
            .print('fg=' + str(self.fg.area)) \
            .print('hand=' + str(self.hand.area)) \
            .print('fg/hand=' + str((self.fg.area / self.hand.area) if self.hand.area > 0 else None)) \
            .print('product=' + str(self.product is not None)) \
            .draw(self.image)

    @property
    def touching(self) -> bool:
        if not self.location.touching:
            return False
        if not self.hand_found:
            return False

        x_prev, y_prev = self[-1].hand.tip
        x_now, y_now = self[0].hand.tip        
        return y_prev < y_now

    @property
    def entering(self) -> bool:
        return self.hand_found_at(0) and not self.hand_found_at(-1)

    @property
    def exiting(self) -> bool:
        return not self.hand_found_at(0) and self.hand_found_at(-1)

    def hand_found_at(self, index) -> bool:
        return \
            self[index-0].hand_found or \
            self[index-1].hand_found or \
            self[index-2].hand_found or \
            self[index-3].hand_found

    @lazyprop
    def hand_found(self) -> bool:
        return self.hand.found and self.fg.area > 0 and 0 < self.hand.area < 500000  

    @property
    def last_with_hand(self) -> bool:
        return next((f for f in self._history if f.hand_found and f.product), \
            next((f for f in self._history if f.hand_found))) 
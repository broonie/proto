from typing import Callable, List
from .image import Image
from .image_source import ImageSource
from .layout import Layout
from .side import Side
from .frame import Frame
from .background import Background

import rx
from rx.subjects import Subject


class FrameSource(Subject):
    def __init__(self, source: ImageSource, side: Side):
        super().__init__()        
        self._bg = Background(source.read())
        self._layout = Layout(side)
        self.history = []
        source.subscribe(lambda img: self._layout.update(img))        
        source \
            .select(self._on_image) \
            .subscribe(self)        

    def skip(self, count):
        for x in range(count):
            self.read()

    def read(self):
        return list(self.first().to_blocking())[0]

    def _on_image(self, image: Image):
        frame = Frame(image, self._bg, self._layout, self.history)
        self.history.insert(0, frame)
        del self.history[10:]
        return frame

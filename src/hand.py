import sys
import os
import numpy as np
import cv2 as cv
from .image import Image
from .stopwatch import Stopwatch

from .mark import Mark
from functools import reduce

from .lazyprop import lazyprop

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

mask_rcnn_dir = os.path.abspath("lib/Mask_RCNN")
sys.path.append(mask_rcnn_dir)

from mrcnn.config import Config
from mrcnn import model as modellib

weights_h5_path = 'model/mask_rcnn_hand_0100.h5'

class HandConfig(Config):
   """Configuration for training on the hands dataset.
   Derives from the base Config class and overrides some values.
   """
   # Give the configuration a recognizable name
   NAME = "hand"

   # We use a GPU with 12GB memory, which can fit two images.
   # Adjust down if you use a smaller GPU.
   IMAGES_PER_GPU = 1

   # Number of classes (including background)
   NUM_CLASSES = 1 + 1  # Background + hand

   # Number of training steps per epoch
   STEPS_PER_EPOCH = 100

   # Skip detections with < 80% confidence
   DETECTION_MIN_CONFIDENCE = 0.8
    
class InferenceConfig(HandConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    IMAGE_MIN_DIM = 400
    IMAGE_MAX_DIM = 512

class Hand:
    model = None

    def init():
        if(Hand.model is None):
            with Stopwatch('Hand model') as sw:
                config = InferenceConfig()
                Hand.model = modellib.MaskRCNN(mode="inference", config=config, model_dir="")
                Hand.model.load_weights(weights_h5_path, by_name=True)

    def __init__(self, image: Image):
        Hand.init()
        self.image = image
        with Stopwatch("Hand detection") as sw:
            self.detected = Hand.model.detect([image.matrix], verbose=0)[0]
        
    @lazyprop
    def found(self):
        return self.one_mask and self.on_edge

    @lazyprop
    def one_mask(self):
        return len(self.detected['rois']) == 1 

    @lazyprop
    def on_edge(self):
        w, h = self.image.size
        return np.any(self.detected['masks'][h // 20,:,0])

    @lazyprop 
    def area(self) -> int:    
        return np.count_nonzero(self.detected['masks']) if self.found else 0

    def clip(self, image: Image) -> Image:
        if not self.found:
            return image

        mask = np.invert(self.detected['masks']).astype(np.uint8)
        mask = np.concatenate((mask, mask, mask), axis = 2) 
        return Image(np.multiply(image.matrix, mask))
    
    @lazyprop
    def mark(self) -> Mark:
        if(not self.found):
            return Mark.empty()

        mask = self.detected['masks'].astype(np.uint8)
        mask = np.multiply(255, mask)

        _, thresh = cv.threshold(mask, 254, 255,0)
        contours, __ = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
        
        if(not self.found):
            return Mark.empty()

        return reduce(lambda mark, shelf: mark.polygon((0, 0, 255), self.image, shelf), [poly for contour in contours for poly in contour], Mark.empty()) \
            .print('on_edge=' + str(self.on_edge)) \
            .print('tip=' + str(self.tip))        

    def save(self, path: str) -> None:
        mask = self.detected['masks'].astype(np.uint8)
        mask = np.concatenate((mask, mask, mask), axis = 2) * 255
        Image(mask).save(path)

    @property
    def tip(self) -> (int, int):
        if not self.found:
            return (0,0)

        m = cv.split(self.detected['masks'].astype(np.uint8))[0]
        x, y = np.nonzero(m)
        if len(x) == 0:
            return (0,0)

        i = np.argmax(x)
        return (x[i], y[i]) 

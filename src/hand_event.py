from enum import Enum
from .location import Location
from .product import Product
from .mark import Mark

class EventType(Enum):
    enter = 1
    move = 2
    exit = 3
    take = 4
    put = 5

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name

class HandEvent:
    def __init__(self, type: EventType, location: Location, product: Product, mark: Mark):
        super().__init__()
        self.type = type
        self.location = location
        self.product = product
        self.mark = mark

    def define(self, product: Product):
        return HandEvent(self.type, self.location, product, self.mark)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '%s - %s - %s' % (self.type, self.location, self.product)

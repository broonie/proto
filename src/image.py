import numpy as np
import cv2 as cv
from .lazyprop import lazyprop

class Image:
    ## https://docs.scipy.org/doc/numpy/reference/generated/numpy.matrix.html
    ## It is no longer recommended to use matrix class, even for linear algebra. 
    ## Instead use regular arrays. The class may be removed in the future.
    def __init__(self, matrix: np.ndarray):
        from .mark import Mark
        self.matrix = matrix
        self.mark = Mark.empty()

    def load(path: str):
        return Image(cv.imread(path))

    def save(self, path: str) -> None:
        cv.imwrite(path, self.matrix)

    def __getitem__(self, pt: (int, int)):
        x,y = pt
        return self.matrix[x,y]
             
    def __add__(self, image):
        return Image(np.concatenate((self.matrix, image.matrix), 1))

    def polygon(self, bgr:(int, int, int), points):
        w, h = self.size

        ## https://docs.opencv.org/3.1.0/dc/da5/tutorial_py_drawing_functions.html
        pts = np.array([[p[0] * w, p[1] * h] for p in points], np.int32)
        pts = pts.reshape((-1,1,2))
        cv.polylines(self.matrix, [pts], True, bgr, 3)

    def print(self, text: str):
        y = 10
        for line in text.split('\n'):
            w, h = cv.getTextSize(line, cv.FONT_HERSHEY_SIMPLEX, 1, 2)[0]
            y += int(1.5 * h)
            cv.putText(self.matrix, line, (10, y), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)

    def clone(self):
        return Image(np.copy(self.matrix))

    def clone(self):
        return Image(np.copy(self.matrix))

    def resize(self, k: float):
        return Image(cv.resize(self.matrix, tuple(int(k*x) for x in self.size), interpolation = cv.INTER_LINEAR))

    @property
    def size(self):
        return (self.matrix.shape[1], self.matrix.shape[0])

    @lazyprop 
    def area(self) -> int:
        b, g, r = cv.split(self.matrix)
        return np.count_nonzero(b + g + r)

    @lazyprop
    def roi(self) -> (int, int, int, int):
        b, g, r = cv.split(self.matrix)
        x, y = np.nonzero(b + g + r)
        if len(x) == 0:
            return (0,0,0,0)

        top = np.min(x)
        bottom = np.max(x)
        left = np.min(y)
        right = np.max(y)

        return (left, top, right, bottom) 

    @lazyprop
    def cropped(self):
        left, top, right, bottom = self.roi
        return Image(self.matrix[top:bottom, left:right])

from typing import Callable, List
from .image import Image

from threading import Thread
import sys
import rx
from rx.subjects import Subject
from rx.concurrency import EventLoopScheduler
from rx.core import Observable
from time import time

import cv2
from cv2 import VideoCapture
from arlo import Arlo
from itertools import count


class ImageSource(Subject):
    def arlo(cameraId: int, login: str, passwd: str):
        return ArloSource(cameraId, login, passwd)

    def file(path: str):
        return FileSource(path)

    def __init__(self):
        super().__init__()

    def read(self):
        return list(self.first().to_blocking())[0]

class CaptureSource(ImageSource):
    def __init__(self, capture: VideoCapture):
        super().__init__()        
        self.capture = capture
        fps = self.capture.get(cv2.CAP_PROP_FPS)
        start = time()
        Observable.range(0, sys.maxsize, EventLoopScheduler()) \
            .select(lambda i: time() - start) \
            .select(lambda sec: int(fps * sec)) \
            .take_while(lambda n: n < self.capture.get(cv2.CAP_PROP_FRAME_COUNT)) \
            .select(self._read) \
            .take_while(lambda img: img is not None) \
            .subscribe(self)

    def __del__(self):
        self.capture.release()

    def _read(self, n):
        self.capture.set(cv2.CAP_PROP_POS_FRAMES, n)
        ok, frame = self.capture.read()
        return Image(frame) if ok else None

class FileSource(CaptureSource):
    def __init__(self, path: str):
        super().__init__(VideoCapture(path))

class ArloSource(CaptureSource):
    def __init__(self, cameraId: int, login: str, passwd: str):
        arlo = Arlo(login, passwd)
        basestations = arlo.GetDevices('basestation')
        cameras = arlo.GetDevices('camera')
        streamUrl = arlo.StartStream(basestations[0], cameras[cameraId])
        super().__init__(VideoCapture(streamUrl))


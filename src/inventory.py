from typing import List, Dict
import threading

from .location import Location
from .shelf_bin import ShelfBin

class Inventory:
    def __init__(self):
        self.bins = {}

    def add(self, product: str, location: Location) -> None:
        self.bins[location] = self.bins[location].add(product) if location in self.bins else ShelfBin(location, [product])

    def remove(self, product: str, location: Location) -> None:
        if location in self.bins:
            self.bins[location] = self.bins[location].remove(product)

    def list(self) -> List[ShelfBin]:
        return list(self.bins.values())

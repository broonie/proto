from .location import Location
from .product import Product
from .image import Image
from .frame import Frame
from .console import Console
from .catalog import Catalog
from .tracker import Tracker, TrackerEvent, EnterEvent, ExitEvent, TouchEvent

import rx
from rx.subjects import Subject

Thresh = 0.55

class Labeler(Subject):
    def __init__(self, tracker: Tracker, catalog: Catalog, console: Console):
        super().__init__()
        self._catalog = catalog
        self._console = console
        self._location = Location.outside()
        self._product = Product.none()
        self._name = None       
        self._enter = None 
        tracker.subscribe(self._on_e)
        console.subscribe(self._on_name)

    def _on_name(self, name: str):
        self._name = name

    def _on_e(self, e: TrackerEvent):
        if isinstance(e, EnterEvent) and e.frame.product is not None:
            self._enter = e.frame
            self._product = self._sync(e.frame.product)

        if isinstance(e, TouchEvent):
            self._location = e.frame.location

        if isinstance(e, ExitEvent):
            if self._location.touching:
                if e.frame.product is None: 
                    if self._product != Product.none():
                        self.on_next(PutEvent(self._product.name, self._location, self._enter))
                else:
                    self._product = self._sync(e.frame.product)
                    self.on_next(TakeEvent(self._product.name, self._location, e.frame))

            self._name = None
            self._location = Location.outside()
            self._product = Product.none()
            self._enter = None
            
    def _sync(self, photo: Image) -> Product:
        product = Product.unknown().capture(photo)
        p, m = self._catalog.match(product)
        if m < Thresh:
            return self._catalog.add(product.define(self._name or self._console.prompt('Eh?')))                
        else:
            return self._catalog.replace(p, product.define(self._name or p.name).merge(p))

class LabelerEvent:
    def __init__(self, product: str, location: Location, frame: Frame):
        super().__init__()
        self.product = product
        self.location = location
        self.frame = frame

    def __str__(self):
        return '%s %s at %s' % (type(self).__name__, self.product, self.location)

class PutEvent(LabelerEvent):
    def __init__(self, product: str, location: Location, frame: Frame):
        super().__init__(product, location, frame)

class TakeEvent(LabelerEvent):
    def __init__(self, product: str, location: Location, frame: Frame):
        super().__init__(product, location, frame)


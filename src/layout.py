import numpy as np
import cv2
import sys

from .location import Location
from .position import Position
from .side import Side
from .image import Image
from .mark import Mark
from functools import reduce

def distanceFromPointToSegment(p, p1, p2):
    angle1 = np.dot(p - p1, p2 - p1)
    angle2 = np.dot(p - p2, p1 - p2)

    if angle1 < 0:
        distance = np.linalg.norm(p1 - p)
    elif angle2 < 0:
        distance = np.linalg.norm(p2 - p)
    else:
        distance = np.linalg.norm(np.cross(p2 - p1, p1 - p)) / np.linalg.norm(p2 - p1) 

    return distance

def distanceBetweenSegments(a1, b1, a2, b2):
    dist = 0

    if not intersect(a1, b1, a2, b2):
        dist1 = distanceFromPointToSegment(a1, a2, b2)
        dist2 = distanceFromPointToSegment(b1, a2, b2)
        dist3 = distanceFromPointToSegment(a2, a1, b1)
        dist4 = distanceFromPointToSegment(b2, a1, b1)

        dist = min(dist1, dist2, dist3, dist4)
    
    return dist

def intersect(a1, b1, a2, b2):
    a = (b2[0] - a2[0])*(a1[1] - a2[1]) - (b2[1] - a2[1])*(a1[0] - a2[0])
    b = (b1[0] - a1[0])*(a1[1] - a2[1]) - (b1[1] - a1[1])*(a1[0] - a2[0])
    c = (b2[1] - a2[1])*(b1[0] - a1[0]) - (b2[0] - a2[0])*(b1[1] - a1[1])

    if ((c != 0) and (a/c >=0) and (a/c <= 1) and (b/c >=0) and (b/c <= 1)) or ((c == 0) and (a == 0) and (b == 0)):
        return True
    else:
        return False

def correctShelvesCoords(shelves, frame):
    height, width, channels = frame.shape

    correct_shelves = []
    if shelves is not None:
        min_y = sys.maxsize
        for shelf in shelves:
            min_y = min(min_y, shelf[0][1], shelf[1][1])

        for i in range (len(shelves)):
            #shelf = shelves[i]
            shelf = np.array([shelves[i][0], shelves[i][1]])

            x1, y1 = shelf[0]
            x2, y2 = shelf[1]

            if (x1 != x2):
                k = (y2-y1)/(x2-x1)
                b = y1 - k*x1

                if shelf[1][1] > min_y:
                    shelf[1][0] = (min_y - b)/k
                    shelf[1][1] = min_y
            
                if (shelf[0][0] < (height - shelf[0][1])) and (shelf[0][0] < (width - shelf[0][0])):
                    shelf[0][0] = 0
                    shelf[0][1] = k*0 + b   
                elif ((width - shelf[0][0]) < (height - shelf[0][1])) and ((width - shelf[0][0]) < shelf[0][0]):
                    shelf[0][0] = width
                    shelf[0][1] = k*width + b
                else:
                    shelf[0][0] = (height - b)/k
                    shelf[0][1] = height                 
            
                correct_shelves.append(shelf)
    
    return correct_shelves #shelves

def findLines(img):  
    img = cv2.GaussianBlur(img, (3, 3), 1)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    h, s, v = cv2.split(hsv)
    v = cv2.convertScaleAbs(v)

    blur = cv2.GaussianBlur(v, (3, 3), 3)
    #blur = cv2.GaussianBlur(hsv, (3, 3), 3)
    #blur = cv2.GaussianBlur(v, (7, 7), 1)
    #cv2.imshow('blur', blur)

    canny = cv2.Canny(blur, 50, 150, apertureSize = 3)
    #cv2.imshow('Canny', canny)

    lines = cv2.HoughLinesP(canny, 1, np.pi/180, threshold = 10, minLineLength = 50, maxLineGap = 3)
    
    return lines


def swap(p1, p2):
    if p1[1] < p2[1]:
        return [p2, p1]
    else:
        return [p1, p2]
    

def updateShelves(shelves, frame):
    KX = 5 # 7
    KY = 3 # 2

    height, width, channels = frame.shape
    width = width // KX 
    height = height // KY     
    img = cv2.resize(frame, (width, height), interpolation = cv2.INTER_LINEAR)

    lines = findLines(img)
    if (lines is None):
        return shelves    
    
    for x1, y1, x2, y2 in lines[0]:
        p1 = np.array([x1, y1])
        p2 = np.array([x2, y2])
        p1ns = np.array([x1*KX, y1*KY])
        p2ns = np.array([x2*KX, y2*KY])
        
        line_len = np.linalg.norm(p1 - p2)
        #if ((line_len > width / 5) & ((y1+y2)/2 > height/2)):
        if ((line_len > height / 3) & ((y1+y2)/2 > height/2) & (min(y1,y2) > height/2)):
            if shelves == []: 
                shelves = [[p1ns, p2ns]]
            else:
                min_distance = 1000
                for i in range (len(shelves)):
                    shelf = shelves[i]
                    p1os = np.array([shelf[0][0]/KX, shelf[0][1]/KY])
                    p2os = np.array([shelf[1][0]/KX, shelf[1][1]/KY])
                    '''if intersect(p1os, p2os, p1, p2):
                        dist = 0
                    else:'''
                    dist = distanceBetweenSegments(p1os, p2os, p1, p2)
                    min_distance = min(min_distance, dist)

                    if (dist < 10):
                    #if (dist < 30):
                        if (np.linalg.norm(p1os - p2os) < line_len):
                            #shelves[i] = [p1ns, p2ns]
                            shelves[i] = swap(p1ns, p2ns)

                if min_distance > 70:
                    #shelves.append([p1ns, p2ns])
                    shelves.append(swap(p1ns, p2ns))
    return shelves
    
def getColorMap(shelvesCount):
    colorMap = []

    n = shelvesCount + 1
    for i in range (n):
        # color_left = (255 - int((i+1)*255/(n+1)), 0, 0)
        # color_centre = (0, 255 - int((i+1)*255/(n+1)), 0)
        # color_right = (0, 0, 255 - int((i+1)*255/(n+1)))

        color_left = (i + 1, 0, 0)
        color_centre = (0, i + 1, 0)
        color_right = (0, 0, i + 1)

        colorMap.append((color_left, color_centre, color_right))

    return colorMap

# def drawColorMap(colorMap, height, width):
#     bitMap = 255 * np.ones((width, height, 3), np.uint8)

#     n = len(colorMap) - 1
#     for i in range (n):
#         cv2.line(bitMap, (0, int((i+1)*width/(n+1))), (height, int((i+1)*width/(n+1))), (0, 0, 0), 3)

#     for j in range (2):
#         cv2.line(bitMap, (int((j+1)*height/3), 0), (int((j+1)*height/3), width), (0, 0, 0), 3)

#     for i in range (n + 1):
#         for j in range (3):
#             cv2.floodFill(bitMap, None, (int((j+0.5)*height/3), int((i+0.5)*width/(n+1))), colorMap[i][j])

#     return bitMap             

def test_x(x, width):
    if (x < 0):
        return 0
    elif (x > width):
        return width
    else:
        return x    

def getPantryMap(shelves, height, width, colorMap, side):
    far = 0.20
    centre = 0.55
    near = 0.25

    pantryMap = np.zeros((height, width, 3), np.uint8)

    if (len(shelves) == 0):
        return pantryMap

    ny = height - shelves[0][1][1]
    y_far = shelves[0][1][1]    
    y_centre = y_far + ny*far
    y_near = y_centre + ny*centre

    x_far = [0, width]
    x_centre = [0, width]
    x_near = [0, width]
    x_height = [0, width]
    for shelf in shelves:
        cv2.line(pantryMap, (shelf[0][0], shelf[0][1]), (shelf[1][0], shelf[1][1]), (0, 0, 0), 3)

        x1, y1 = shelf[0]
        x2, y2 = shelf[1]

        k = (y2-y1)/(x2-x1)
        b = y1 - k*x1

        x_far.append((y_far - b)/k)
        x_centre.append((y_centre - b)/k)
        x_near.append((y_near - b)/k)
        x_height.append((height - b)/k) 

    x_far.sort()
    x_centre.sort()
    x_near.sort()
    x_height.sort()

    
    n = len(x_far) - 1
    for i in range (n):           
        x1f = test_x(x_far[i], width)
        x2f = test_x(x_far[i+1], width)
        x1c = test_x(x_centre[i], width)
        x2c = test_x(x_centre[i+1], width)
        x1n = test_x(x_near[i], width)
        x2n = test_x(x_near[i+1], width)        
        x1h = test_x(x_height[i], width)
        x2h = test_x(x_height[i+1], width)              

        if (side == Side.left):
            color_far = colorMap[i][2] 
            color_centre = colorMap[i][1] 
            color_near = colorMap[i][0] 
        elif (side == Side.right):
            color_far = colorMap[n - i - 1][0] 
            color_centre = colorMap[n - i - 1][1] 
            color_near = colorMap[n - i - 1][2] 
        else:
            raise Exception('Unknown side.')

        cv2.fillConvexPoly(pantryMap, np.array([[x1f, y_far], [x2f, y_far], [x2c, y_centre], [x1c, y_centre]], np.int32), color_far)
        cv2.fillConvexPoly(pantryMap, np.array([[x1c, y_centre], [x2c, y_centre], [x2n, y_near], [x1n, y_near]], np.int32), color_centre)
        cv2.fillConvexPoly(pantryMap, np.array([[x1n, y_near], [x2n, y_near], [x2h, height], [x1h, height]], np.int32), color_near)                

    return pantryMap   


class Layout:
    def __init__(self, side: Side):
        super().__init__()
        self._side = side
        self._shelves = []
        self._map = None  
        self.mark = Mark.empty()

    def update(self, image: Image):
        self._shelves = updateShelves(self._shelves, image.matrix)
        corrected_shelves = correctShelvesCoords(self._shelves, image.matrix)
        if(len(corrected_shelves) == 0):
            return

        height, width, channels = image.matrix.shape
        colorMap = getColorMap(len(corrected_shelves))
        self._map = Image(getPantryMap(corrected_shelves, height, width, colorMap, self._side))
        self.mark = reduce(lambda mark, shelf: mark.polygon((255, 0, 0), image, shelf), corrected_shelves, Mark.empty())
    
    def __getitem__(self, pt: (int, int)) -> Location:
        if(self._map is None):
            return Location.air()

        bgr = self._map[pt]
        if(bgr[0] != 0):
            return Location(self._map[pt][0] - 1, Position.left)
        if(bgr[1] != 0):
            return Location(self._map[pt][1]  - 1, Position.center)
        if(bgr[2] != 0):
            return Location(self._map[pt][2] - 1, Position.right)

        return Location.air()

#from __future__ import annotations
from .position import Position

class Location:	
    # hand is on the picture outside of does not touch shelves
    def air():
        return Location(-1, Position.none)

    # hand is not on the picture
    def outside():
        return Location(-2, Position.none)

    def __init__(self, shelf: int, position: Position):
        self.shelf = shelf
        self.position = position
        self._shelf_names = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth']

    @property
    def touching(self):
        return self.shelf >= 0

    def __eq__(self, other):
        return self.shelf == other.shelf and self.position == other.position

    def __hash__(self):
        return hash(self.shelf) ^ hash(self.position)
        
    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if(self == Location.air()):
            return "air"

        if(self == Location.outside()):
            return "outside"

        return "%s shelf, %s" % ( \
            self._shelf_names[self.shelf] if self.shelf >= 0 and self.shelf < len(self._shelf_names) else self.shelf, \
            self.position)
  

    

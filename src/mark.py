import numpy as np
import cv2 as cv
from .image import Image

class Mark:
    def empty():
        return Mark([], '')

    def __init__(self, actions, text):
        self.actions = actions
        self.text = text

    def polygon(self, bgr:(int, int, int), image: Image, points):
        w, h = image.size
        pts = [[p[0] / w, p[1] / h] for p in points]
        action = lambda img: img.polygon(bgr, pts)
        return Mark(self.actions + [action], self.text)

    def print(self, text: str):
        return Mark(self.actions, self.text + text + '\n')

    def draw(self, image: Image) -> Image:
        copy = image.clone()
        copy.print(self.text)
        for action in self.actions:
            action(copy)

        return copy

    def __add__(self, mark):
        return Mark(self.actions + mark.actions, self.text + mark.text)


from enum import Enum

class Position(Enum):
    none = 0
    left = 1
    center = 2
    right = 3	

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name


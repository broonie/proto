from .descriptor import Descriptor
from .image import Image

class Product:
    def none():
        return Product('None', Descriptor.none())

    def unknown():
        return Product('Unknown', Descriptor.none())

    def __init__(self, name: str, descriptor: Descriptor):
        self.name = name
        self.descriptor = descriptor

    def define(self, name: str):
        return Product(name, self.descriptor)

    def capture(self, image: Image):
        return Product(self.name, self.descriptor + Descriptor.sift(image))

    def match(self, other):
        return self.descriptor.match(other.descriptor)

    def merge(self, other):
        return Product(self.name, self.descriptor + other.descriptor)

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)
        
    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name
#from __future__ import annotations
from typing import List
from .location import Location

class ShelfBin:
    def __init__(self, location: Location, products: List[str] = []):
        self.location = location
        self.products = products

    # def add(self, product: str) -> ShelfBin:
    def add(self, product: str):
        return ShelfBin(self.location, self.products[:] + [product])

    # def remove(self, product: str) -> ShelfBin:
    def remove(self, product: str):
        return ShelfBin(self.location, \
            [p for p in self.products if p != product] + [p for p in self.products if p == product][1:])

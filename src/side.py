from enum import Enum

class Side(Enum):
    left = 1
    right = 2	

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name


from time import time
from os import remove

class Stopwatch:
    try:
        remove('log.txt')
    except OSError:
        pass

    def __init__(self, name):
        self.started = time()
        self.name = name
    
    def log(self, message = ''):
        elapsed = time() - self.started
        with open("log.txt", "a") as log:
            log.write('[%s] %s: %d ms\n' % (self.name, message, int(1000 * elapsed)))

    def __enter__(self):
        pass

    def __exit__(self, type, value, traceback):
        self.log('completed')
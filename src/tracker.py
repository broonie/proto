from .frame import Frame
from .frame_source import FrameSource
from .location import Location
from .image import Image
from .background import Background

import rx
from rx.subjects import Subject

class Tracker(Subject):
    def __init__(self, source: FrameSource):
        super().__init__()
        source.skip(4)
        source.subscribe(self._on_frame)

    def _on_frame(self, frame: Frame):
        if frame.entering:
            self.on_next(EnterEvent(frame.last_with_hand))
        elif frame.exiting:
            self.on_next(ExitEvent(frame.last_with_hand))
        elif frame.touching:
            self.on_next(TouchEvent(frame))
        else:
            self.on_next(TrackerEvent(frame))

class TrackerEvent:
    def __init__(self, frame: Frame):
        super().__init__()
        self.frame = frame

    def __str__(self):
        return '%s at %s' % (type(self).__name__ , self.frame.location)

class EnterEvent(TrackerEvent):
    def __init__(self, frame: Frame):
        super().__init__(frame)

class ExitEvent(TrackerEvent):
    def __init__(self, frame: Frame):
        super().__init__(frame)        

class TouchEvent(TrackerEvent):
    def __init__(self, frame: Frame):
        super().__init__(frame)

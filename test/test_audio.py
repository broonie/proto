from .context import src
from src.console import Console

import unittest
import time

class TestAudio(unittest.TestCase):
    def integration_test_audio(self):        
        # import ptvsd
        # ptvsd.enable_attach()
        # ptvsd.wait_for_attach()
        # ptvsd.break_into_debugger()

        self.console = Console.audio(9000)
        self.console.subscribe(self.on_speach)
        time.sleep(120)

    def on_speach(self, text: str):
        print(text)
        self.console.write(text)


if __name__ == '__main__':
    unittest.main()
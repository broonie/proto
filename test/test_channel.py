from .context import src
from src.image_source import ImageSource
from src.frame_source import FrameSource
from src.labeler import Labeler, LabelerEvent, TakeEvent, PutEvent
from src.side import Side
from src.hand import Hand
from src.tracker import Tracker
from src.catalog import Catalog
from src.console import Console

import time
import unittest

class TestChannel(unittest.TestCase):
    def integration_test_channel(self):    

        # import ptvsd
        # ptvsd.enable_attach()
        # ptvsd.wait_for_attach()
        # ptvsd.break_into_debugger()

        self.counter = 0
        isrc = ImageSource.file('test/res/LOnly2.mp4')
        fsrc = FrameSource(isrc, Side.left)
        tracker = Tracker(fsrc)
        tracker.subscribe(self.on_tracker_e)
        console =  Console.test(['Cereal', 'Soda', 'Hot Chocolate', 'Soda', 'Waffles', 'Teryaki sauce', 'Candy bar', 'Baking chips', 'Peanut butter', 'Olives', 'UNKNOWN'])
        labeler = Labeler(tracker, Catalog(), console)
        labeler.subscribe(self.on_e)
        time.sleep(5*60)

    def on_tracker_e(self, e):
        self.counter = self.counter + 1
        fn = 'test/ch/tracker/%04i %s.png' % (self.counter, e)
        e.frame.mark.resize(0.3).save(fn)

    def on_e(self, e):
        self.counter = self.counter + 1
        fn = 'test/ch/%04i %s.png' % (self.counter, e)
        e.frame.mark.resize(0.3).save(fn)

if __name__ == '__main__':
    unittest.main()
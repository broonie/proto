from .context import src
from src.image_source import ImageSource
from src.channel import Channel
from src.catalog import Catalog
from src.console import Console
from src.event_source import EventSource
from src.hand_event import EventType, HandEvent
from src.side import Side

import time
import unittest

class TestEvents(unittest.TestCase):
    def integration_test_events(self):
        EventSource.pantry( \
            Channel.stable(Side.left, ImageSource.file('test/res/1L.mp4'), 0), \
            Console.text(), \
            Catalog(), \
            0.55) \
            .subscribe(print)
            
        time.sleep(10)


if __name__ == '__main__':
    unittest.main()
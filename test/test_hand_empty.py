from .context import src
from src.hand import Hand
from src.image import Image

import unittest

class TestHandEmpty(unittest.TestCase):
    
    def test_product(self):
        product = [('test/mask/bgclip/handclip-%d.png' % i) for i in range(17, 18)]
        for fn in product:
            hand = Hand(Image.load(fn))
            self.assertFalse(hand.empty)

    def test_empty(self):
        empty = [('test/mask/bgclip/handclip-%d.png' % i) for i in range(12)]
        for fn in empty:
            hand = Hand(Image.load(fn))
            self.assertTrue(hand.empty)


if __name__ == '__main__':
    unittest.main()
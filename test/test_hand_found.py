from .context import src
from src.hand import Hand
from src.image import Image

import unittest

class TestHandFound(unittest.TestCase):
    def test_hand_found(self):
        image = Image.load('test/res/raw/product1_angle1_right.png')
        hand = Hand(image)
        self.assertTrue(hand.found)
 
    def test_hand_not_found(self):
        image = Image.load('test/res/no-hand/no-hand.png')
        hand = Hand(image)
        self.assertFalse(hand.found)


if __name__ == '__main__':
    unittest.main()
from .context import src
import unittest
	
class TestInventory(unittest.TestCase):
    def test_add_remove_list(self):
        inventory = src.Inventory()
        location1 = src.Location(2, src.Position.left)
        location2 = src.Location(1, src.Position.center)
        inventory.add('Pasta', location1)
        self.assertTrue(len(inventory.list()) == 1 and inventory.bins[location1].products == ['Pasta'])
        inventory.add('Pasta', location2)
        inventory.add('Jam', location2)
        inventory.remove('Jam', location2)
        inventory.remove('Pasta', location1)
        self.assertTrue((len(inventory.list()) == 2) and (inventory.bins[location2].products == ['Pasta']))
        inventory.remove('Pasta', location2)
        self.assertTrue(len(inventory.list()) == 2)

if __name__ == '__main__':
    unittest.main()

from .context import src
from src.image_source import ImageSource
from src.image import Image
import unittest
import time
import threading
import cv2
from src.location import Location
from src.position import Position
from src.side import Side
from src.layout import Layout

class TestLayout(unittest.TestCase):
    def test_layout(self):        
        layout = Layout(Side.left)

        def on_image(image: Image):
            layout.update(image)
            layout.mark.draw(image) \
                .save('test/mark/layout.png')

        isrc = ImageSource.file('test/res/1L.mp4')
        isrc.subscribe(on_image)
        time.sleep(3)

        self.assertEqual(layout[(0,0)], Location.air())
        self.assertEqual(layout[(1000,500)], Location(1, Position.left))
        self.assertEqual(layout[(1000,1900)], Location(2, Position.left))
        self.assertEqual(layout[(900, 800)], Location(1, Position.center))
        self.assertEqual(layout[(600, 950)], Location(1, Position.right))
        

        
if __name__ == '__main__':
    unittest.main()
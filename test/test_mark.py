from .context import src
from src.image import Image
import unittest
import cv2
from src.layout import Layout
from src.mark import Mark
from src.background import Background
from src.image_source import ImageSource
from src.hand import Hand

class TestMark(unittest.TestCase):
    def image_polygon(self):        
        image = Image.load('test/res/raw/product1_angle1_right.png')
        image.polygon((0, 0, 255), [[0,0], [0.5, 0], [0.5, 0.25], [0, 0.25] ])
        image.save('test/mark/poly.png')
        
    def mark_polygon(self):
        image = Image.load('test/res/raw/product1_angle1_right.png')        
        mark = Mark.empty() \
            .print('Hello') \
            .print('World') \
            .polygon((0, 0, 255), image, [[0,0], [1000, 0], [1000, 500], [0, 500] ])

        mark.draw(image) \
            .save('test/mark/mark.png')

    def mark_background(self):
        isrc = ImageSource.file('test/res/1L.mp4')
        bg = Background(isrc.read())
        for i in range(10):
            for j in range(10):
                isrc.read()
            image = bg.extract(isrc.read())
            bg.mark.draw(image) \
                .save('test/mark/bg%04i.png' % i)

    def mark_hand(self):
        image = Image.load('test/res/raw/product1_angle1_right.png')
        hand = Hand(image)
        hand.mark.draw(image) \
            .save('test/mark/hand.png')

                

if __name__ == '__main__':
    unittest.main()
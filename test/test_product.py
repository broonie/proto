from .context import src
from src.hand import Hand
from src.image import Image
from src.descriptor import Descriptor
from src.product import Product
import unittest

def image(f, p, a):
    pathL = ''.join(['test/res/', f, '/p', str(p), '_a', str(a), '_L.png'])
    pathR = ''.join(['test/res/', f, '/p', str(p), '_a', str(a), '_R.png'])
    img = Image.load(pathL) + Image.load(pathR)
    return img.resize(0.25)

class TestProduct(unittest.TestCase):
    def test_product_distinguishable(self):
        p1 = Product.none() \
            .capture(image('no-hand', 1, 1)) \
            .capture(image('no-hand', 1, 2)) \

        p2 = Product.none() \
            .capture(image('no-hand', 2, 1)) \
            .capture(image('no-hand', 2, 2))

        match = p1.match(p2) > 0.55
        self.assertFalse(match)

    def test_product1_matching(self):
        p1_1 = Product.none() \
            .capture(image('no-hand', 1, 1))

        p1_2 = Product.none() \
            .capture(image('no-hand', 1, 2)) \

        match = p1_1.match(p1_2) > 0.55
        self.assertTrue(match)

    def test_product2_matching(self):
        p2_1 = Product.none() \
            .capture(image('no-hand', 2, 1))

        p2_2 = Product.none() \
            .capture(image('no-hand', 2, 2)) \

        match = p2_1.match(p2_2) > 0.55
        self.assertTrue(match)

if __name__ == '__main__':
    unittest.main()